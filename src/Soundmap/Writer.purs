-- File Name: Writer.purs
-- Description: Writer
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 20 Nov 2023 13:47:19
-- Last Modified: 24 Nov 2023 22:58:29

module Soundmap.Writer where

import Prelude

import Data.Argonaut.Encode (class EncodeJson, encodeJson)
import Data.Map as Map
import Data.Maybe (Maybe(..), fromMaybe)
import Data.String (joinWith)
import Data.Traversable (traverse)
import Data.Tuple (Tuple(..))
import Effect (Effect)
import Foreign (Foreign, unsafeToForeign)
import Soundmap.Sample (Sample(..), fromMidi)

--  taken from https://github.com/nonbili/purescript-template-literals
foreign import _template :: String -> Foreign -> Effect String

template :: forall a. EncodeJson (Record a) => String -> Record a -> Effect String
template tpl params = _template tpl (unsafeToForeign $ encodeJson params)

data OutputType = SFZ | DecentSampler

derive instance eqOut :: Eq OutputType

type SampleSettings = Map.Map String String
type SampleOut =
  { name :: String
  , note :: String
  , lo :: String
  , hi :: String
  , lovel :: String
  , hivel :: String
  , rr :: String
  , sl :: String
  }

sample2rec :: Sample -> SampleOut
sample2rec (Sample s) =
  { name: s.fileName
  , note: show s.keyNumber
  , lo: show $ (fromMidi s.midi).low
  , hi: show $ (fromMidi s.midi).high
  , lovel: show $ (fromMidi s.velocity).low
  , hivel: show $ (fromMidi s.velocity).high
  , rr: show $ fromMaybe 1 s.roundRobin
  , sl: show $ fromMaybe 1 s.seqLength
  }

defaultSettings :: SampleSettings
defaultSettings =
  Map.fromFoldable $
    [ Tuple "ampeg_attack" "0.0"
    , Tuple "ampeg_decay" "0.0"
    , Tuple "ampeg_release" "0.001"
    , Tuple "ampeg_delay" "0.0"
    , Tuple "ampeg_sustain" "100.0"
    , Tuple "loop_mode" "no_loop"
    ]

writeGlobalSFZ :: SampleSettings -> String
writeGlobalSFZ settings =
  joinWith "\n"
    $ map (\(Tuple k v) -> k <> "=" <> v)
    $ Map.toUnfoldable settings

writeGlobalDS :: SampleSettings -> String
writeGlobalDS settings =
  joinWith " "
    $ map (\(Tuple k v) -> k <> "=" <> "\"" <> v <> "\"")
    $ Map.toUnfoldable settings

writeSampleSFZ :: Sample -> Maybe SampleSettings -> Effect String
writeSampleSFZ sample settings =
  flip template
    (sample2rec sample)
    ( "<region> sample=${name} pitch_keycenter=${note} "
        <> "lokey=${lo} hikey=${hi} "
        <> "lovel=${lovel} hivel=${hivel} "
        <> "seq_length=${sl} seq_position=${rr}"
        <> if opcodes == "" then "" else " " <> opcodes
    )
  where
  opcodes = case settings of
    Just s ->
      joinWith " "
        $ map (\(Tuple k v) -> k <> "=" <> v)
        $ Map.toUnfoldable s
    Nothing -> ""

writeSampleDS :: Sample -> Maybe SampleSettings -> Effect String
writeSampleDS sample settings =
  flip template
    (sample2rec sample)
    ( "      <sample path=\"./${name}\" rootNote=\"${note}\" "
        <> "loNote=\"${lo}\" hiNote=\"${hi}\" "
        <> "loVel=\"${lovel}\" hiVel=\"${hivel}\" "
        <> "seqLength=\"${sl}\" seqPosition=\"${rr}\""
        <> if opcodes == "" then ">" else " " <> opcodes <> ">"
    )
  where
  opcodes = case settings of
    Just s ->
      joinWith " "
        $ map (\(Tuple k v) -> k <> "=" <> "\"" <> v <> "\"")
        $ Map.toUnfoldable s
    Nothing -> ""

-- | basic writer
-- | https://sfzformat.com/
-- | https://www.decentsamples.com/docs/format-documentation.html
write :: OutputType -> SampleSettings -> Array Sample -> Map.Map Int SampleSettings -> Effect String
write out global arr settings = do
  samples <- traverse (\eff -> eff) samplesEff
  pure
    $ header
        <> (if out == SFZ then "\n\n" else "\n")
        <> joinWith "\n" samples
        <> (if out == SFZ then "" else "\n    </group>\n  </group>\n</DecentSampler>")
  where
  header = case out of
    SFZ ->
      "// Automapped using https://codeberg.org/KMIJPH/soundmap\n"
        <> "<control>\n"
        <> "default_path=./\n\n"
        <> "<global\n"
        <> "<group>\n"
        <> writeGlobalSFZ global
    DecentSampler ->
      "<!-- Automapped using https://codeberg.org/KMIJPH/soundmap -->\n"
        <> "<DecentSampler>\n"
        <> "  <groups>\n"
        <> "    <group "
        <> writeGlobalDS global
        <> ">"
  samplesEff = flip map arr \(Sample s) -> case out of
    SFZ -> writeSampleSFZ (Sample s) (Map.lookup s.keyNumber settings)
    DecentSampler -> writeSampleDS (Sample s) (Map.lookup s.keyNumber settings)

-- File Name: Note.purs
-- Description: Note types
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 20 Nov 2023 12:30:57
-- Last Modified: 21 Nov 2023 12:06:38

module Soundmap.Note where

import Prelude

import Data.Array (concat, filter, index, range)
import Data.Maybe (Maybe(..))
import Data.Newtype (class Newtype)

data KeyName = C | D | E | F | G | A | B

derive instance eqName :: Eq KeyName
instance showName :: Show KeyName where
  show name = case name of
    C -> "C"
    D -> "D"
    E -> "E"
    F -> "F"
    G -> "G"
    A -> "A"
    B -> "B"

data Signature = Flat | Sharp | None

derive instance eqSignature :: Eq Signature
instance showSignature :: Show Signature where
  show sig = case sig of
    Flat -> "b"
    Sharp -> "#"
    None -> ""

newtype Key = Key
  { name :: KeyName
  , sig :: Signature
  }

derive instance newtypeKey :: Newtype Key _
instance showKey :: Show Key where
  show (Key k) = show k

instance eqKey :: Eq Key where
  eq k1 k2 =
    case equality k1 k2 of
      true -> true
      false -> equality k2 k1
    where
    equality (Key kk1) (Key kk2) =
      case kk1 of
        { name: C, sig: Flat } -> if kk1 == kk2 || kk2 == { name: B, sig: None } then true else false
        { name: C, sig: Sharp } -> if kk1 == kk2 || kk2 == { name: D, sig: Flat } then true else false
        { name: D, sig: Sharp } -> if kk1 == kk2 || kk2 == { name: E, sig: Flat } then true else false
        { name: E, sig: Sharp } -> if kk1 == kk2 || kk2 == { name: F, sig: None } then true else false
        { name: F, sig: Flat } -> if kk1 == kk2 || kk2 == { name: E, sig: None } then true else false
        { name: F, sig: Sharp } -> if kk1 == kk2 || kk2 == { name: G, sig: Flat } then true else false
        { name: G, sig: Sharp } -> if kk1 == kk2 || kk2 == { name: A, sig: Flat } then true else false
        { name: A, sig: Sharp } -> if kk1 == kk2 || kk2 == { name: B, sig: Flat } then true else false
        { name: B, sig: Sharp } -> if kk1 == kk2 || kk2 == { name: C, sig: None } then true else false
        _ -> kk1 == kk2

newtype Note = Note
  { key :: Key
  , oct :: Int
  , keyNumber :: Int
  }

derive instance newtypeNote :: Newtype Note _
derive instance eqNote :: Eq Note

instance showNote :: Show Note where
  show (Note n) = show n

type KeyMap = Array Note

-- | creates keymap from the reference C4
keymap :: Int -> KeyMap
keymap c4 =
  filter (\(Note n) -> n.keyNumber >= 0 && n.keyNumber <= 127) (concat octaves)
  where
  keyNumberLow = c4 - 72
  octaveLow = (-2)
  notes =
    [ Note { key: Key { name: C, sig: None }, oct: octaveLow, keyNumber: keyNumberLow }
    , Note { key: Key { name: C, sig: Sharp }, oct: octaveLow, keyNumber: keyNumberLow + 1 }
    , Note { key: Key { name: D, sig: None }, oct: octaveLow, keyNumber: keyNumberLow + 2 }
    , Note { key: Key { name: D, sig: Sharp }, oct: octaveLow, keyNumber: keyNumberLow + 3 }
    , Note { key: Key { name: E, sig: None }, oct: octaveLow, keyNumber: keyNumberLow + 4 }
    , Note { key: Key { name: F, sig: None }, oct: octaveLow, keyNumber: keyNumberLow + 5 }
    , Note { key: Key { name: F, sig: Sharp }, oct: octaveLow, keyNumber: keyNumberLow + 6 }
    , Note { key: Key { name: G, sig: None }, oct: octaveLow, keyNumber: keyNumberLow + 7 }
    , Note { key: Key { name: G, sig: Sharp }, oct: octaveLow, keyNumber: keyNumberLow + 8 }
    , Note { key: Key { name: A, sig: None }, oct: octaveLow, keyNumber: keyNumberLow + 9 }
    , Note { key: Key { name: A, sig: Sharp }, oct: octaveLow, keyNumber: keyNumberLow + 10 }
    , Note { key: Key { name: B, sig: None }, oct: octaveLow, keyNumber: keyNumberLow + 11 }
    ]
  octaves = map
    ( \i ->
        map
          ( \(Note n) ->
              Note { key: n.key, oct: n.oct + i, keyNumber: n.keyNumber + (12 * i) }
          )
          notes
    )
    (range octaveLow 12)

-- | retrieves the keynumber for a keymap for a given note
fromNote :: Note -> KeyMap -> Maybe Int
fromNote (Note n) kmap =
  case index f 0 of
    Just (Note e) -> Just e.keyNumber
    Nothing -> Nothing
  where
  f = flip filter kmap \(Note r) ->
    case n.key of
      Key { name: B, sig: Sharp } -> (r.key == n.key) && (r.oct == n.oct + 1)
      Key { name: C, sig: Flat } -> (r.key == n.key) && (r.oct == n.oct - 1)
      _ -> (r.key == n.key) && (r.oct == n.oct)

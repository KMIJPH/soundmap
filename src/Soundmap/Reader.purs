-- File Name: Reader.purs
-- Description: Filename reader
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 20 Nov 2023 13:16:51
-- Last Modified: 23 Nov 2023 12:40:46

module Soundmap.Reader where

import Prelude

import Control.Alt ((<|>))
import Data.Array (concat, toUnfoldable, fromFoldable, many)
import Data.Either (Either(..))
import Data.List (List(..), (:))
import Data.Maybe (Maybe(..))
import Data.String.CodeUnits (fromCharArray)
import Data.Tuple (Tuple(..))
import Parsing (Parser, runParser)
import Parsing.Combinators (optionMaybe, sepBy)
import Parsing.String (char, eof)
import Parsing.String.Basic (intDecimal, noneOf, oneOf)
import Soundmap.Note (Key(..), KeyMap, KeyName(..), Note(..), Signature(..), fromNote)
import Soundmap.Sample (Midi(..))
import Soundmap.Sample as S

-- | parses attribute array
attrs :: Parser String (Array String)
attrs = do
  listChars <- (many $ noneOf [ '_', '.' ]) `sepBy` (char '_' <|> char '.')
  pure $ fromFoldable $ map (\e -> fromCharArray e) listChars

-- | parses an integer
int :: Parser String Int
int = do
  i <- intDecimal
  _ <- eof
  pure $ i

-- | parses a key
key :: Parser String KeyName
key = do
  k <- oneOf $ concat [ keys, keysL ]
  case k of
    'D' -> pure D
    'd' -> pure D
    'E' -> pure E
    'e' -> pure E
    'F' -> pure F
    'f' -> pure F
    'G' -> pure G
    'g' -> pure G
    'A' -> pure A
    'a' -> pure A
    'B' -> pure A
    'b' -> pure A
    _ -> pure C
  where
  keys = [ 'C', 'D', 'E', 'F', 'G', 'A', 'B' ]
  keysL = [ 'c', 'd', 'e', 'f', 'g', 'a', 'b' ]

-- | parses a signature
sig :: Parser String Signature
sig = do
  s <- optionMaybe $ oneOf [ '#', 'b' ]
  case s of
    Just '#' -> pure Sharp
    Just 'b' -> pure Flat
    _ -> pure None

-- | parses a note
note :: Parser String Note
note = do
  k <- key
  s <- sig
  octave <- intDecimal
  _ <- eof
  pure $ Note
    { key: Key { name: k, sig: s }
    , oct: octave
    , keyNumber: 0
    }

-- | parses a note or a keynumber
keynumber :: String -> KeyMap -> Maybe Int
keynumber input' kmap =
  case runParser input' note of
    Right n -> fromNote n kmap
    Left _ -> case runParser input' int of
      Right i -> Just i
      Left _ -> Nothing

-- | parses a dynamic
dynamic :: Parser String (Maybe S.Dynamic)
dynamic = do
  pre <- oneOf dynamics
  main <- optionMaybe $ oneOf dynamics
  _ <- eof
  case Tuple pre main of
    Tuple 'p' (Just 'p') -> pure $ Just S.PP
    Tuple 'm' (Just 'p') -> pure $ Just S.MP
    Tuple 'm' (Just 'f') -> pure $ Just S.MF
    Tuple 'f' (Just 'f') -> pure $ Just S.FF
    Tuple 'p' Nothing -> pure $ Just S.P
    Tuple 'f' Nothing -> pure $ Just S.F
    _ -> pure Nothing
  where
  dynamics = [ 'm', 'p', 'f' ]

-- | parses a filename to a sample
-- | a note (either string key or keynumber) is required
-- | order:
-- | - keynumber or key string
-- | - dynamic
-- | - round robin
sample :: String -> KeyMap -> Parser String (Maybe S.Sample)
sample input kmap = do
  parts <- attrs
  let
    s = S.Sample
      { dynamic: Nothing
      , fileName: input
      , keyNumber: 404
      , roundRobin: Nothing
      , seqLength: Nothing
      , midi: S.Midi { low: 0, high: 0 }
      , velocity: S.Midi { low: 0, high: 127 }
      }
  let parsed = parser (toUnfoldable parts) s
  case getKN parsed of
    404 -> pure Nothing
    _ -> pure $ Just parsed

  where
  getKN :: S.Sample -> Int
  getKN (S.Sample s') = s'.keyNumber

  parser :: List String -> S.Sample -> S.Sample
  parser input' (S.Sample sample') =
    case input' of
      (Nil) -> S.Sample sample'
      (p : next) -> do
        case Tuple sample'.keyNumber (keynumber p kmap) of
          Tuple 404 (Just res) -> parser next (S.Sample $ sample' { keyNumber = res, midi = Midi { low: res, high: res } })
          _ -> case Tuple sample'.dynamic (runParser p dynamic) of
            Tuple Nothing (Right res) -> parser next (S.Sample $ sample' { dynamic = res })
            _ -> case Tuple sample'.roundRobin (runParser p int) of
              Tuple Nothing (Right res) -> parser next (S.Sample $ sample' { roundRobin = Just res })
              _ -> parser next (S.Sample sample')

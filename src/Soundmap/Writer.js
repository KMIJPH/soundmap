"use strict";

// adapted from https://github.com/nonbili/purescript-template-literals
export function _template(tpl){
    return function(params) {
        return function() {
            return new Function(
                "{" + Object.keys(params).join(",") + "}",
                "return `" + tpl + "`"
            )(params);
        }
    }
}

-- File Name: Sample.purs
-- Description: Sample types
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 20 Nov 2023 13:51:24
-- Last Modified: 24 Nov 2023 18:50:58

module Soundmap.Sample where

import Prelude

import Data.Array (catMaybes, concat, filter, fromFoldable, length, nub, range, sort, toUnfoldable)
import Data.List (List(..), (:), head)
import Data.Map as Map
import Data.Maybe (Maybe(..))
import Data.Newtype (class Newtype)
import Data.Tuple (Tuple(..), fst)

newtype Midi = Midi
  { low :: Int
  , high :: Int
  }

derive instance eqMidi :: Eq Midi
derive instance newtypeMidi :: Newtype Midi _
instance showMidi :: Show Midi where
  show (Midi v) = show v.low <> " - " <> show v.high

instance semiringMidi :: Semiring Midi where
  add (Midi m1) (Midi m2) = Midi { low: m1.low + m2.low, high: m1.high + m2.high }
  zero = Midi { low: 0, high: 0 }
  mul (Midi m1) (Midi m2) = Midi { low: m1.low * m2.low, high: m1.high * m2.high }
  one = Midi { low: 1, high: 1 }

instance ordMidi :: Ord Midi where
  compare (Midi m1) (Midi m2) =
    if m1.low > m2.low then GT
    else if m1.low < m2.low then LT
    else EQ

fromMidi :: Midi -> { low :: Int, high :: Int }
fromMidi (Midi m) = { low: m.low, high: m.high }

-- creates an array of every midi value (lower to upper)
midiValues :: List Midi -> Array Int
midiValues arr = concat $ fromFoldable $ map (\(Midi m) -> range m.low m.high) arr

data Dynamic = PP | P | MP | MF | F | FF

instance eqDynamic :: Eq Dynamic where
  eq PP PP = true
  eq P P = true
  eq MP MP = true
  eq MP MF = true
  eq MF MP = true
  eq MF MF = true
  eq F F = true
  eq FF FF = true
  eq _ _ = false

derive instance ordDynamic :: Ord Dynamic
instance showDynamic :: Show Dynamic where
  show dyn = case dyn of
    PP -> "pp"
    P -> "p"
    MP -> "mp"
    MF -> "mf"
    F -> "f"
    FF -> "ff"

newtype Sample = Sample
  { dynamic :: Maybe Dynamic
  , fileName :: String
  , keyNumber :: Int
  , roundRobin :: Maybe Int
  , seqLength :: Maybe Int
  , velocity :: Midi
  , midi :: Midi
  }

derive instance eqSample :: Eq Sample
derive instance newtypeSample :: Newtype Sample _
instance showSample :: Show Sample where
  show (Sample s) = show s

type VelocityMap = Map.Map Dynamic Midi

defaultVelocities :: { pp :: Midi, p :: Midi, mp :: Midi, f :: Midi, ff :: Midi }
defaultVelocities =
  { pp: (Midi { low: 0, high: 25 })
  , p: (Midi { low: 26, high: 51 })
  , mp: (Midi { low: 52, high: 77 })
  , f: (Midi { low: 78, high: 103 })
  , ff: (Midi { low: 104, high: 127 })
  }

defaultVelocityMap :: VelocityMap
defaultVelocityMap =
  Map.fromFoldable
    [ Tuple PP default.pp
    , Tuple P default.p
    , Tuple MP default.mp
    , Tuple F default.f
    , Tuple FF default.ff
    ]
  where
  default = defaultVelocities

-- | sets dynamics to given velocities
mapVelocity :: Sample -> VelocityMap -> Sample
mapVelocity (Sample sample) velmap =
  case sample.dynamic of
    Nothing -> Sample sample
    Just dyn -> case Map.lookup dyn velmap of
      Just midi -> Sample (sample { velocity = midi })
      Nothing -> case dyn == MF of
        true -> case Map.lookup MP velmap of
          Just midi -> Sample (sample { velocity = midi })
          Nothing -> Sample sample
        false -> Sample sample

-- | 'fixes' a velocity map by stretching the velocities
fixVelocities :: VelocityMap -> VelocityMap
fixVelocities velmap =
  mapper k Map.empty
  where
  k :: List Dynamic
  k = map fst $ Map.toUnfoldable velmap

  mapper :: List Dynamic -> VelocityMap -> VelocityMap
  mapper list velmap' =
    case list of
      (Nil) -> velmap'
      (this : Nil) -> case Map.lookup this velmap of
        Just (Midi m) -> Map.insert this (Midi { low: m.low, high: 127 }) velmap'
        Nothing -> velmap'
      (this : next) -> case Map.lookup this velmap of
        Just (Midi l) -> case head next of
          Just n -> case Map.lookup n velmap of
            Just (Midi h) -> mapper next $ Map.insert this (Midi { low: l.low, high: h.low - 1 }) velmap'
            Nothing -> velmap'
          Nothing -> velmap'
        Nothing -> velmap'

type MidiMap = Map.Map Int Midi

-- | creates a midimap where each keynumber is stretched to the next
midiMap :: Array Int -> MidiMap
midiMap arr =
  mapper unique Map.empty
  where
  unique :: List Int
  unique = toUnfoldable $ sort $ nub $ arr

  first :: Maybe Int
  first = head unique

  mapper :: List Int -> MidiMap -> MidiMap
  mapper list midimap =
    case list of
      (Nil) -> midimap
      (this : next) -> case head next of
        Just n -> mapper next (Map.insert this (Midi { low: lower, high: n - 1 }) midimap)
        Nothing -> mapper next (Map.insert this (Midi { low: lower, high: 127 }) midimap)
        where
        lower = if first == Just this then 0 else this

-- | 'stretches' keynumbers according to a midimap
stretch :: Sample -> MidiMap -> Sample
stretch (Sample sample) midimap =
  case Map.lookup sample.keyNumber midimap of
    Just k -> Sample $ sample { midi = k }
    Nothing -> Sample sample

-- | 'fixes' round robins by remapping the round robins in ascending order
-- | starting from 1
fixRobins :: Maybe Int -> Array Int -> Maybe Int
fixRobins sampleRR robins =
  mapper ordered 1 sampleRR

  where
  ordered :: List Int
  ordered = toUnfoldable $ sort $ robins

  mapper :: List Int -> Int -> Maybe Int -> Maybe Int
  mapper list index rr =
    case rr of
      Nothing ->
        case list of
          (Nil) -> Just index
          (_ : next) -> mapper next (index + 1) Nothing
      Just r -> case list of
        (Nil) -> Just r
        (this : next) -> case r == this of
          true -> Just index
          false -> mapper next (index + 1) rr

-- | filters samples that have the same keynumber and robin
sameRobin :: Array Sample -> Sample -> Array Sample
sameRobin arr' (Sample ref) = filter (\(Sample s) -> s.keyNumber == ref.keyNumber && s.roundRobin == ref.roundRobin) arr'

-- | filters samples that have the same keynumber and dynamic
sameDynamic :: Array Sample -> Sample -> Array Sample
sameDynamic arr' (Sample ref) = filter (\(Sample s) -> s.keyNumber == ref.keyNumber && s.dynamic == ref.dynamic) arr'

-- | processes the samples based on the settings
process :: Array Sample -> Boolean -> VelocityMap -> Array Sample
process arr stretch' velmap =
  map (\s -> mapVelocity s velmap') mapped
  where
  keynumbers = map (\(Sample s) -> s.keyNumber) arr
  midimap = midiMap keynumbers
  stretched = case stretch' of
    true -> map (\s -> stretch s midimap) arr
    false -> arr
  fixed = map
    ( \(Sample s) -> Sample $
        s
          { roundRobin = fixRobins s.roundRobin
              $ catMaybes
              $ map (\(Sample ss) -> ss.roundRobin)
              $ sameDynamic stretched (Sample s)
          , seqLength = case length $ sameDynamic stretched (Sample s) of
              0 -> Nothing
              e -> Just e
          }
    )
    stretched
  mapped = map (\s -> mapVelocity s velmap) fixed
  velmap' = fixVelocities
    $ Map.fromFoldable
    $ catMaybes
    $ map
        ( \(Sample s) -> case s.dynamic of
            Just dyn -> Just $ Tuple dyn s.velocity
            Nothing -> Nothing
        )
        mapped


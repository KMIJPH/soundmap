-- File Name: Main.purs
-- Description: Automapper for SFZ (and later also DecentSampler)
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 20 Nov 2023 11:07:43
-- Last Modified: 24 Nov 2023 15:13:04

module Main where

import Prelude

import Data.Array (catMaybes, elem, filter)
import Data.Const (Const)
import Data.Either (hush)
import Data.Foldable (traverse_)
import Effect (Effect)
import Effect.Aff.Class (class MonadAff)
import Halogen as H
import Halogen.Aff (awaitLoad, runHalogenAff, selectElement)
import Halogen.HTML as HH
import Halogen.VDom.Driver (runUI)
import Parsing (runParser)
import Soundmap.Note (keymap)
import Soundmap.Reader (sample)
import Soundmap.Sample (Sample(..))
import Type.Proxy (Proxy(..))
import UI.FileView as ChildFile
import UI.MapView as ChildMap
import UI.Style as Style
import Web.DOM.ParentNode (QuerySelector(..))

data View = FileView | MapView

type State =
  { mapped :: Array Sample
  , unmapped :: Array String
  , view :: View
  }

data Action
  = DoNothing
  | GotoMap (Array String)

type Slots =
  ( file :: H.Slot (Const Void) ChildFile.Message Int
  , map :: H.Slot (Const Void) ChildMap.Message Int
  )

_file = Proxy :: Proxy "file"
_map = Proxy :: Proxy "map"

initialState :: forall i. i -> State
initialState _ =
  { mapped: []
  , unmapped: []
  , view: FileView
  }

component :: forall q i o m. MonadAff m => H.Component q i o m
component =
  H.mkComponent
    { initialState
    , render
    , eval: H.mkEval $ H.defaultEval { handleAction = handleAction }
    }

render :: forall m. MonadAff m => State -> H.ComponentHTML Action Slots m
render state = case state.view of
  FileView -> HH.slot _file 0 (ChildFile.component) unit listenFile
  MapView -> HH.slot _map 0 (ChildMap.component) { mapped: state.mapped, unmapped: state.unmapped } listenMap

listenFile :: ChildFile.Message -> Action
listenFile message = case message of
  ChildFile.Content files -> GotoMap files
  ChildFile.NoContent -> DoNothing

listenMap :: ChildMap.Message -> Action
listenMap message = case message of
  ChildMap.Yup -> DoNothing

handleAction :: forall o m. MonadAff m => Action -> H.HalogenM State Action Slots o m Unit
handleAction action = case action of
  GotoMap files -> do
    H.modify_ \s -> s { mapped = mapped, unmapped = unmapped, view = MapView }
    where
    parsed = map (\f -> hush $ runParser f (sample f (keymap 60))) files
    mapped = catMaybes $ catMaybes parsed
    filenames = map (\(Sample s) -> s.fileName) mapped
    unmapped = filter (\f -> not $ elem f filenames) files
  DoNothing -> pure unit

main :: Effect Unit
main = runHalogenAff do
  awaitLoad
  traverse_ (runUI Style.component unit) =<< selectElement (QuerySelector "head")
  traverse_ (runUI component unit) =<< selectElement (QuerySelector ".vdom-content")

-- File Name: Style.purs
-- Description: CSS style stuff
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 21 Nov 2023 19:23:28
-- Last Modified: 24 Nov 2023 22:46:03

module UI.Style where

import Prelude

import CSS ((|*))
import CSS as C
import CSS.Color as Col
import CSS.Common as Common
import CSS.Cursor as Cursor
import CSS.Flexbox as Flexbox
import CSS.Overflow as Overflow
import CSS.Selector as CS
import CSS.TextAlign as CT
import CSS.VerticalAlign as VA
import Data.String (Pattern(..), Replacement(..), replace)
import Halogen as H
import Halogen.HTML.CSS as HS

type Classes =
  { fileSelector :: String -- upload file selector
  , boxed :: String
  , buttonAdd :: String
  , buttonRemove :: String
  , buttonSmall :: String
  , piano :: String -- piano container
  , pianoKeys :: String -- keys container
  , pianoKey :: String -- a key
  , pianoKeyWhite :: String
  , pianoKeyBlack :: String
  , pianoKeyLast :: String -- white keys that border with another white key
  , pianoKeyMapped :: String -- mapped keys
  , pianoKeyRange :: String -- selected key range
  , pianoKeySelected :: String -- selected key
  , fieldDiv :: String
  , formButton :: String
  , navigation :: String -- navigation section
  , navigationLeft :: String
  , navigationRight :: String
  , mapBoxOuter :: String -- mapped / unmapped files container
  , mapBoxInner :: String -- same but inner
  , mapUL :: String -- an ul in the map box
  }

classes :: Classes
classes =
  { fileSelector: "vcard-file-selector"
  , boxed: "soundmap-boxed"
  , buttonAdd: "soundmap-button-add"
  , buttonRemove: "soundmap-button-remove"
  , buttonSmall: "soundmap-button-small"
  , piano: "soundmap-piano"
  , pianoKeys: "soundmap-piano-keys"
  , pianoKey: "soundmap-piano-key"
  , pianoKeyWhite: "soundmap-piano-white"
  , pianoKeyBlack: "soundmap-piano-black"
  , pianoKeyLast: "soundmap-piano-last"
  , pianoKeyMapped: "soundmap-piano-mapped"
  , pianoKeySelected: "soundmap-piano-selected"
  , pianoKeyRange: "soundmap-piano-range"
  , fieldDiv: "soundmap-field-div"
  , formButton: "soundmap-form-button"
  , navigation: "soundmap-navigation"
  , navigationLeft: "soundmap-navigation-left"
  , navigationRight: "soundmap-navigation-right"
  , mapBoxOuter: "soundmap-mapbox-outer"
  , mapBoxInner: "soundmap-mapbox-inner"
  , mapUL: "soundmap-map-ul"
  }

type Colors =
  { bg :: Col.Color
  , selected :: Col.Color
  , whiteUnmapped :: Col.Color
  , blackUnmapped :: Col.Color
  , whiteRange :: Col.Color
  , blackRange :: Col.Color
  , transparent :: Col.Color
  }

colors :: Colors
colors =
  { bg: Col.rgb 0 0 0
  , selected: Col.rgb 150 210 210
  , whiteUnmapped: Col.rgb 115 115 115
  , blackUnmapped: Col.rgb 80 80 80
  , whiteRange: Col.rgb 86 129 196
  , blackRange: Col.rgb 52 81 125
  , transparent: Col.rgba 0 0 0 0.0
  }

type SVG =
  { path :: String
  , viewbox :: String
  }

type Buttons =
  { add :: SVG
  , remove :: SVG
  }

svgPaths :: Buttons
svgPaths =
  { add: { path: "M256 80c0-17.7-14.3-32-32-32s-32 14.3-32 32V224H48c-17.7 0-32 14.3-32 32s14.3 32 32 32H192V432c0 17.7 14.3 32 32 32s32-14.3 32-32V288H400c17.7 0 32-14.3 32-32s-14.3-32-32-32H256V80z", viewbox: "0 0 448 512" }
  , remove: { path: "M342.6 150.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L192 210.7 86.6 105.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L146.7 256 41.4 361.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0L192 301.3 297.4 406.6c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L237.3 256 342.6 150.6z", viewbox: "0 0 384 512" }
  }

createSvg :: SVG -> String -> String
createSvg { path, viewbox } fill =
  "data:image/svg+xml;utf8,"
    <> "<svg "
    <> "fill='"
    <> replace (Pattern "#") (Replacement "%23") fill
    <> "' "
    <> "xmlns='http://www.w3.org/2000/svg' "
    <> "preserveAspectRatio='xMidYMid meet' "
    <> "viewBox='"
    <> viewbox
    <> "'>"
    <> "<path d='"
    <> path
    <> "' /></svg>"

buttonSvg :: String -> SVG -> C.StyleM Unit
buttonSvg class_ svg = do
  C.select (C.Selector (C.byClass class_) C.Star)
    ( do
        (C.backgroundImage $ C.url $ createSvg svg (Col.toHexString colors.whiteRange))
        C.backgroundSize C.contain
    )
  C.select (CS.with ((C.Selector (C.byClass class_) C.Star)) (C.pseudo "hover"))
    ( do
        (C.backgroundImage $ C.url $ createSvg svg (Col.toHexString colors.selected))
    )

component :: forall q i o m. H.Component q i o m
component =
  H.mkComponent
    { initialState: const unit
    , render
    , eval: H.mkEval $ H.defaultEval
    }

render :: forall m. Unit -> H.ComponentHTML Unit () m
render _ =
  HS.stylesheet $ do
    C.select (C.html)
      ( do
          Overflow.overflowY Overflow.overflowAuto
          C.padding (C.px 10.0) (C.px 10.0) (C.px 10.0) (C.px 10.0)
      )
    -- navigation
    -- everything inside the navigation div
    C.select (C.Selector (C.byClass classes.navigation) C.Star)
      ( do
          C.position (C.Position $ C.fromString "sticky")
          C.top (C.px 4.0)
          C.marginBottom (C.px 10.0)
          C.display C.flex
          C.alignItems $ Flexbox.AlignItemsValue Common.center
      )
    -- right part of navigation
    C.select (C.Selector (C.byClass classes.navigationRight) C.Star)
      ( do
          C.display C.flex
          C.marginLeft Common.auto
          C.marginRight $ C.px 0.0
      )

    -- mapView
    C.select (C.Selector (C.byClass classes.boxed) C.Star)
      ( do
          Overflow.overflow Overflow.overflowAuto
          C.borderTop C.solid (C.px 1.0) colors.bg
          C.padding (C.px 4.0) (C.px 4.0) (C.px 4.0) (C.px 4.0)
          C.marginBottom $ C.px 4.0
      )
    C.select (C.Selector (C.byClass classes.mapBoxOuter) C.Star) $
      ( do
          C.display C.flex
          C.justifyContent C.spaceAround
      )
    C.select (C.Selector (C.byClass classes.mapUL) C.Star) $
      ( do
          Overflow.overflow Overflow.overflowAuto
          C.maxHeight (C.px 300.0)
      )
    C.select (C.Selector (C.byClass classes.fieldDiv) C.Star |* C.label)
      ( do
          C.display C.inlineBlock
          C.width (C.px 140.0)
          CT.textAlign CT.leftTextAlign
      )
    C.select (C.Selector (C.byClass classes.mapBoxInner) C.Star |* C.span)
      ( do
          C.fontSize $ C.pct 110.0
          C.fontWeight C.bold
      )
    -- a single form field div
    C.select C.form
      ( do
          C.alignItems $ Flexbox.AlignItemsValue Common.center
      )
    -- piano
    C.select (C.Selector (C.byClass classes.piano) C.Star) $
      ( do
          Overflow.overflow Overflow.overflowAuto
      )
    C.select (C.Selector (C.byClass classes.pianoKeys) C.Star) $
      ( do
          C.display C.flex
          C.key (C.fromString "list-style") "none"
          C.height (C.px 80.0)
          C.width (C.px 1500.0)
          C.margin (C.px 10.0) Common.auto (C.px 10.0) Common.auto
      )
    C.select (C.Selector (C.byClass classes.pianoKey) C.Star) $
      ( do
          C.cursor Cursor.Pointer
          C.position C.relative
      )
    C.select (C.Selector (C.byClass classes.pianoKey) C.Star |* C.span) $
      ( do
          C.position C.absolute
          C.bottom $ C.pct 1.0
          C.width $ C.pct 90.0
          CT.textAlign CT.center
          C.fontSize $ C.pct 65.0
      )
    C.select (C.Selector (C.byClass classes.pianoKeyLast) C.Star) $
      ( do
          C.borderRight C.solid (C.px 1.0) C.black
      )
    C.select (C.Selector (C.byClass classes.pianoKeyBlack) C.Star) $
      ( do
          C.zIndex 2
          C.width $ C.px 44.0
          C.height $ C.pct 65.0
          C.margin (C.pct 0.0) (C.pct (-0.5)) (C.pct 0.0) (C.pct (-0.5))
          C.background $ colors.blackUnmapped
          C.border C.solid (C.px 1.0) C.black
      )
    C.select (CS.with (C.Selector (C.byClass classes.pianoKeyBlack) C.Star) (C.byClass classes.pianoKeyMapped)) $
      ( do
          C.background $ C.black
      )
    C.select (CS.with (C.Selector (C.byClass classes.pianoKeyBlack) C.Star) (C.byClass classes.pianoKeyRange)) $
      ( do
          C.background $ colors.blackRange
      )
    C.select (C.Selector (C.byClass classes.pianoKeyWhite) C.Star) $
      ( do
          C.width $ C.px 70.0
          C.height $ C.pct 95.0
          C.background $ C.white
          C.borderLeft C.solid (C.px 1.0) C.black
          C.borderBottom C.solid (C.px 1.0) C.black
          C.borderTop C.solid (C.px 1.0) C.black
          C.background $ colors.whiteUnmapped
      )
    C.select (CS.with (C.Selector (C.byClass classes.pianoKeyWhite) C.Star) (C.byClass classes.pianoKeyMapped)) $
      ( do
          C.background $ C.white
      )
    C.select (CS.with (C.Selector (C.byClass classes.pianoKeyWhite) C.Star) (C.byClass classes.pianoKeyRange)) $
      ( do
          C.background $ colors.whiteRange
      )
    C.select (C.Selector (C.byId classes.pianoKeySelected) C.Star) $
      ( do
          C.background $ colors.selected
      )

    -- buttons
    C.select C.button
      ( do
          C.cursor Cursor.pointer
      )
    C.select (C.Selector (C.byClass classes.buttonSmall) C.Star) $
      ( do
          C.backgroundRepeat C.noRepeat
          C.backgroundPosition $ C.placed C.sideCenter C.sideCenter
          VA.verticalAlign VA.Middle
          C.width $ C.px 16.0
          C.height $ C.px 16.0
          C.padding (C.px 0.0) (C.px 0.0) (C.px 0.0) (C.px 0.0)
          C.margin (C.px 0.0) (C.px 0.0) (C.px 0.0) (C.px 5.0)
          C.border (C.Stroke $ Common.none) (C.px 0.0) colors.bg
          C.backgroundColor colors.transparent
      )
    C.select (C.Selector (C.byClass classes.formButton) C.Star) $
      ( do
          C.cursor Cursor.pointer
      )
    buttonSvg classes.buttonAdd svgPaths.add
    buttonSvg classes.buttonRemove svgPaths.remove

-- File Name: MapView.purs
-- Description: Main view
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 21 Nov 2023 23:07:59
-- Last Modified: 24 Nov 2023 21:53:12

module UI.MapView where

import Prelude

import Data.Array (concat, filter, index, sortBy)
import Data.Const (Const)
import Data.Either (Either(..))
import Data.List (List(..), (:))
import Data.List as List
import Data.Map as Map
import Data.Maybe (Maybe(..))
import Data.Set as Set
import Data.Tuple (Tuple(..))
import Effect.Class (class MonadEffect)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Parsing (runParser)
import Parsing.String.Basic (intDecimal)
import Soundmap.Sample (Dynamic(..), Midi(..), Sample(..), VelocityMap, defaultVelocities, defaultVelocityMap, fromMidi, process)
import Soundmap.Writer (OutputType(..), SampleSettings, defaultSettings, write)
import Type.Proxy (Proxy(..))
import UI.Components.HelpButtons (helpButtons)
import UI.Components.Piano as ChildPiano
import UI.Components.SettingsForm as ChildSettings
import UI.FileDownload (download)
import UI.Style (classes)

type MapInput =
  { mapped :: Array Sample
  , unmapped :: Array String
  }

type State =
  { c4 :: Int
  , mapped :: Array Sample
  , unmapped :: Array String
  , velocityMap :: VelocityMap
  , currentSample :: Maybe Sample
  , global :: SampleSettings
  , sampleSettings :: Map.Map Int SampleSettings
  , filesHidden :: Boolean
  , globalHidden :: Boolean
  , stretch :: Boolean
  , output :: OutputType
  }

data Action
  = Receive MapInput
  | Export
  | UpdateC4 String
  | UpdateVelMap Dynamic String
  | UpdateStretch String
  | UpdateSettings Int SampleSettings
  | UpdateOutput String
  | ToggleFiles
  | ToggleGlobal
  | SelectSample Int

data Message = Yup
type Slots =
  ( piano :: H.Slot (Const Void) ChildPiano.Message Int
  , settings :: H.Slot (Const Void) ChildSettings.Message Int
  )

_piano = Proxy :: Proxy "piano"
_settings = Proxy :: Proxy "settings"

initialState :: MapInput -> State
initialState { mapped, unmapped } =
  { unmapped
  , mapped: m
  , c4: 60
  , velocityMap: defaultVelocityMap
  , currentSample: Nothing
  , global: defaultSettings
  , sampleSettings: Map.fromFoldable $ map (\(Sample s) -> Tuple s.keyNumber Map.empty) mapped
  , filesHidden: true
  , stretch: false
  , globalHidden: false
  , output: SFZ
  }
  where
  m = process mapped false Map.empty

component :: forall q m. MonadEffect m => H.Component q MapInput Message m
component =
  H.mkComponent
    { initialState
    , render
    , eval: H.mkEval $ H.defaultEval
        { handleAction = handleAction
        , receive = Just <<< Receive
        }
    }

render :: forall m. MonadEffect m => State -> H.ComponentHTML Action Slots m
render state =
  HH.div_
    [ HH.div [ HP.class_ (HH.ClassName classes.navigation) ]
        [ HH.div [ HP.class_ (HH.ClassName classes.navigationLeft) ]
            [ HH.button [ HE.onClick \_ -> ToggleFiles ] [ if state.filesHidden then HH.text "Show Files" else HH.text "Hide Files" ]
            , HH.button [ HE.onClick \_ -> ToggleGlobal ] [ if state.globalHidden then HH.text "Show Global" else HH.text "Hide Global" ]
            , HH.button [ HE.onClick \_ -> Export ] [ HH.text "Export" ]
            ]
        , helpButtons
        ]
    , HH.slot _piano 0 (ChildPiano.piano)
        { selected: selected
        , c4: state.c4
        , mapped: state.mapped
        }
        listenPiano
    , HH.div [ HP.class_ $ HH.ClassName classes.mapBoxOuter ] mapbox
    , HH.div [ HP.classes [ HH.ClassName classes.boxed, HH.ClassName classes.mapBoxOuter ] ] globalForm
    , HH.div [ HP.classes [ HH.ClassName classes.boxed, HH.ClassName classes.mapBoxOuter ] ] sampleForm
    ]
  where
  selected = case state.currentSample of
    Just (Sample s) -> s.keyNumber
    Nothing -> (-1)
  mapbox = case state.filesHidden of
    false ->
      [ HH.div [ HP.class_ $ HH.ClassName classes.mapBoxInner ]
          [ HH.span_ [ HH.text "Unmapped" ]
          , HH.ul [ HP.class_ $ HH.ClassName classes.mapUL ] (map (\s -> HH.li_ [ HH.text s ]) state.unmapped)
          ]
      , HH.div [ HP.class_ $ HH.ClassName classes.mapBoxInner ]
          [ HH.span_ [ HH.text "Mapped" ]
          , HH.ul [ HP.class_ $ HH.ClassName classes.mapUL ] (map (\(Sample s) -> HH.li_ [ HH.text s.fileName ]) state.mapped)
          ]
      ]
    true -> []
  globalForm = case state.globalHidden of
    false ->
      [ HH.div [ HP.class_ $ HH.ClassName classes.mapBoxInner ]
          [ HH.slot _settings (-1) (ChildSettings.component)
              { target: (-1)
              , settings: state.global
              , name: "Global Opcodes"
              }
              listenSettings
          ]
      , HH.div [ HP.class_ $ HH.ClassName classes.mapBoxInner ]
          [ HH.span_ [ HH.text "Settings" ]
          , HH.div [ HP.class_ $ HH.ClassName classes.fieldDiv ]
              [ HH.label [ HP.for "c4" ] [ HH.text "C4 Keynumber" ]
              , HH.select [ HP.id "c4", HE.onValueChange \v -> UpdateC4 v ]
                  [ HH.option_ [ HH.text "48" ]
                  , HH.option [ HP.selected true ] [ HH.text "60" ]
                  , HH.option_ [ HH.text "72" ]
                  ]
              ]
          , HH.div [ HP.class_ $ HH.ClassName classes.fieldDiv ]
              [ HH.label [ HP.for "stretch" ] [ HH.text "Stretch Samples" ]
              , HH.select [ HP.id "stretch", HE.onValueChange \v -> UpdateStretch v ]
                  [ HH.option_ [ HH.text "true" ]
                  , HH.option [ HP.selected true ] [ HH.text "false" ]
                  ]
              ]
          , HH.div [ HP.class_ $ HH.ClassName classes.fieldDiv ]
              [ HH.label [ HP.for "output" ] [ HH.text "Output Format" ]
              , HH.select [ HP.id "output", HE.onValueChange \v -> UpdateOutput v ]
                  [ HH.option [ HP.selected true ] [ HH.text ".sfz" ]
                  , HH.option_ [ HH.text ".dspreset" ]
                  ]
              ]
          ]
      , HH.div [ HP.class_ $ HH.ClassName classes.mapBoxInner ]
          [ HH.span_ [ HH.text "Velocity Map" ]
          , HH.div_
              [ HH.div [ HP.class_ $ HH.ClassName classes.fieldDiv ]
                  [ HH.label [ HP.for "pp" ] [ HH.text "pp" ]
                  , HH.input (concat [ [ HP.id "pp", HP.value $ show (fromMidi defaultVelocities.pp).high, HE.onValueChange \s -> UpdateVelMap PP s ], midiInput ])
                  ]
              , HH.div [ HP.class_ $ HH.ClassName classes.fieldDiv ]
                  [ HH.label [ HP.for "p" ] [ HH.text "p" ]
                  , HH.input (concat [ [ HP.id "p", HP.value $ show (fromMidi defaultVelocities.p).high, HE.onValueChange \s -> UpdateVelMap P s ], midiInput ])
                  ]
              , HH.div [ HP.class_ $ HH.ClassName classes.fieldDiv ]
                  [ HH.label [ HP.for "mp" ] [ HH.text "mp/mf" ]
                  , HH.input (concat [ [ HP.id "p", HP.value $ show (fromMidi defaultVelocities.mp).high, HE.onValueChange \s -> UpdateVelMap MP s ], midiInput ])
                  ]
              , HH.div [ HP.class_ $ HH.ClassName classes.fieldDiv ]
                  [ HH.label [ HP.for "f" ] [ HH.text "f" ]
                  , HH.input (concat [ [ HP.id "p", HP.value $ show (fromMidi defaultVelocities.f).high, HE.onValueChange \s -> UpdateVelMap F s ], midiInput ])
                  ]
              , HH.div [ HP.class_ $ HH.ClassName classes.fieldDiv ]
                  [ HH.label [ HP.for "ff" ] [ HH.text "ff" ]
                  , HH.input (concat [ [ HP.id "p", HP.value $ show (fromMidi defaultVelocities.ff).high, HE.onValueChange \s -> UpdateVelMap FF s ], midiInput ])
                  ]
              ]
          ]
      ]
    true -> []
  sampleForm = case state.currentSample of
    Just (Sample s) ->
      [ HH.div [ HP.class_ $ HH.ClassName classes.mapBoxInner ]
          [ HH.slot _settings s.keyNumber (ChildSettings.component)
              { target: s.keyNumber
              , settings: case Map.lookup s.keyNumber state.sampleSettings of
                  Nothing -> Map.empty
                  Just m -> m
              , name: "Key" <> show s.keyNumber <> " (" <> show s.midi <> ")"
              }
              listenSettings
          ]
      , HH.div [ HP.class_ $ HH.ClassName classes.mapBoxInner ]
          [ HH.span_ [ HH.text "Files" ]
          , HH.ul [ HP.class_ $ HH.ClassName classes.mapUL ]
              $ map (\(Sample ss) -> showFile (Sample ss) state.velocityMap)
              $ sortBy (\(Sample a) (Sample b) -> compare a.dynamic b.dynamic)
              $ filter (\(Sample a) -> a.keyNumber == s.keyNumber) state.mapped
          ]
      ]
    Nothing -> []
  midiInput = [ HP.type_ HP.InputNumber, HP.min 0.0, HP.max 127.0, HP.step $ HP.Step 1.0 ]

listenPiano :: ChildPiano.Message -> Action
listenPiano message = case message of
  ChildPiano.SendKey i -> SelectSample i

listenSettings :: ChildSettings.Message -> Action
listenSettings message = case message of
  ChildSettings.SendSettings i s -> UpdateSettings i s

handleAction :: forall m. MonadEffect m => Action -> H.HalogenM State Action Slots Message m Unit
handleAction action = case action of
  SelectSample i -> H.modify_ \s -> s { currentSample = flip index 0 $ filter (\(Sample sam) -> sam.keyNumber == i) s.mapped }
  ToggleFiles -> H.modify_ \s -> s { filesHidden = not s.filesHidden }
  ToggleGlobal -> H.modify_ \s -> s { globalHidden = not s.globalHidden }
  Export -> do
    state <- H.get
    output <- H.liftEffect $ write state.output state.global state.mapped state.sampleSettings
    _ <- H.liftEffect $ download output (if state.output == SFZ then "export.sfz" else "export.dspreset") "text/plain"
    pure unit
  UpdateStretch v -> case v of
    "true" -> H.modify_ \state -> state
      { mapped = process state.mapped true state.velocityMap
      , currentSample = Nothing
      , stretch = true
      }
    "false" -> H.modify_ \state -> state
      { mapped = map (\(Sample s) -> Sample $ s { midi = Midi { low: s.keyNumber, high: s.keyNumber } }) state.mapped
      , currentSample = Nothing
      , stretch = false
      }
    _ -> pure unit
  UpdateOutput v -> do
    case v of
      ".sfz" -> H.modify_ \s -> s { output = SFZ }
      ".dspreset" -> H.modify_ \s -> s { output = DecentSampler }
      _ -> pure unit
  UpdateSettings i settings ->
    case i of
      (-1) -> H.modify_ \s -> s { global = settings }
      _ -> H.modify_ \s -> s { sampleSettings = Map.update (\_ -> Just settings) i s.sampleSettings }
  UpdateVelMap dyn v -> do
    previous <- H.get
    new <- case runParser v intDecimal of
      Right n -> case Map.lookup dyn previous.velocityMap of
        Just _ -> H.modify \s -> s { velocityMap = Map.update (\_ -> Just $ Midi { low: 0, high: n }) dyn s.velocityMap }
        Nothing -> H.modify \s -> s { velocityMap = Map.insert dyn (Midi { low: 0, high: n }) s.velocityMap }
      Left _ -> H.modify \s -> s { velocityMap = Map.delete dyn s.velocityMap }
    H.modify_ \s -> s { velocityMap = updateVelMap (List.reverse $ List.sort $ Set.toUnfoldable $ Map.keys new.velocityMap) new.velocityMap }
  UpdateC4 v -> case v of
    "48" -> updateC4 48
    "60" -> updateC4 60
    "72" -> updateC4 72
    _ -> pure unit
  Receive { mapped, unmapped } -> H.modify_ \s -> s
    { mapped = process mapped s.stretch s.velocityMap
    , unmapped = unmapped
    }

-- updates velocity map by using the next upper bound as a lower bound
updateVelMap :: List Dynamic -> VelocityMap -> VelocityMap
updateVelMap list velmap = case list of
  (Nil) -> velmap
  (this : next) -> case Map.lookup this velmap of
    Just (Midi mthis) -> case List.head next of
      Just dyn -> case Map.lookup dyn velmap of
        Just (Midi mnext) -> updateVelMap next $ Map.update (\_ -> Just $ Midi { low: mnext.high + 1, high: mthis.high }) this velmap
        Nothing -> velmap
      Nothing -> updateVelMap next $ Map.update (\_ -> Just $ Midi { low: 0, high: mthis.high }) this velmap
    Nothing -> velmap

-- shows a single file's information
showFile :: forall w i. Sample -> VelocityMap -> HH.HTML w i
showFile (Sample s) velmap =
  HH.li_
    [ HH.text s.fileName
    , HH.ul_
        [ HH.li_ [ HH.text $ "Dynamic: " <> dyn ]
        , HH.li_ [ HH.text $ "Velocity: " <> vel ]
        , HH.li_ [ HH.text $ "Round Robin: " <> rr ]
        ]
    ]
  where
  dyn = case s.dynamic of
    Just d -> show d
    Nothing -> ""
  rr = case s.roundRobin of
    Just r -> show r
    Nothing -> ""
  vel = case s.dynamic of
    Just d -> case Map.lookup d velmap of
      Just m -> show m
      Nothing -> case d of
        MF -> case Map.lookup MP velmap of
          Just mm -> show mm
          Nothing -> ""
        mm -> show mm
    Nothing -> ""

-- updates samples key reference and settings
updateC4 :: forall m. Int -> H.HalogenM State Action Slots Message m Unit
updateC4 c4 = H.modify_ \s -> s
  { currentSample = Nothing
  , c4 = c4
  , sampleSettings = updateSettings (diff s) s.sampleSettings
  , mapped = map
      ( \(Sample x) -> Sample x
          { keyNumber = (x.keyNumber + (diff s))
          , midi = (Midi { low: diff s, high: diff s }) + x.midi
          }
      )
      s.mapped
  }
  where
  diff :: State -> Int
  diff state = state.c4 - c4

  updateSettings :: Int -> Map.Map Int SampleSettings -> Map.Map Int SampleSettings
  updateSettings d settings = Map.fromFoldable new
    where
    oldKeys :: List Int
    oldKeys = Set.toUnfoldable $ Map.keys settings

    oldValues :: List SampleSettings
    oldValues = Map.values settings

    new :: List (Tuple Int SampleSettings)
    new = map (\(Tuple k v) -> Tuple (k + d) v) $ List.zip oldKeys oldValues

-- File Name: HelpButtons.purs
-- Description: Help buttons
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 24 Nov 2023 21:50:56
-- Last Modified: 24 Nov 2023 21:52:44

module UI.Components.HelpButtons where

import Prelude

import Halogen.HTML as HH
import Halogen.HTML.Properties as HP
import UI.Style (classes)

helpButtons :: forall w i. HH.HTML w i
helpButtons =
  HH.div [ HP.class_ (HH.ClassName classes.navigationRight) ]
    [ HH.form [ HP.action "https://sfzformat.com/", HP.target "_blank" ]
        [ HH.input [ HP.class_ $ HH.ClassName classes.formButton, HP.type_ HP.InputSubmit, HP.value "SFZFormat" ] ]
    , HH.form [ HP.action "https://www.decentsamples.com/docs/format-documentation.html", HP.target "_blank" ]
        [ HH.input [ HP.class_ $ HH.ClassName classes.formButton, HP.type_ HP.InputSubmit, HP.value "DSFormat" ] ]
    , HH.form [ HP.action "https://codeberg.org/KMIJPH/soundmap/src/branch/main/README.md", HP.target "_blank" ]
        [ HH.input [ HP.class_ $ HH.ClassName classes.formButton, HP.type_ HP.InputSubmit, HP.value "Help" ] ]
    ]

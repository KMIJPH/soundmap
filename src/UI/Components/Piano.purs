-- File Name: Piano.purs
-- Description: Piano
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 21 Nov 2023 18:21:09
-- Last Modified: 24 Nov 2023 18:30:58

module UI.Components.Piano where

import Prelude

import Data.Array (catMaybes, concat, elem, index, range)
import Data.Const (Const)
import Data.Map as Map
import Data.Maybe (Maybe(..))
import Data.Set as Set
import Data.Tuple (Tuple(..))
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Soundmap.Note (Key(..), KeyMap, KeyName(..), Note(..), Signature(..), keymap)
import Soundmap.Sample (Sample(..), midiValues)
import Type.Proxy (Proxy(..))
import UI.Style (classes)

data Color = Black | White

derive instance eqColor :: Eq Color

type KeyState =
  { key :: Int
  , color :: Color
  , text :: String
  , selected :: Boolean
  , range :: Boolean
  , mapped :: Boolean
  }

data KeyAction = Clicked | ReceiveKey KeyState
data KeyMessage = SendKeynumber Int

pianoKey :: forall q m. H.Component q KeyState KeyMessage m
pianoKey =
  H.mkComponent
    { initialState: \s -> s
    , render
    , eval: H.mkEval $ H.defaultEval
        { handleAction = handleAction
        , receive = Just <<< ReceiveKey
        }
    }
  where

  render :: forall mm. KeyState -> H.ComponentHTML KeyAction () mm
  render state = HH.li
    [ HP.classes
        ( concat
            [ cls
            , if state.color == White then whi else blk
            , if state.mapped then m else []
            , if state.color == White && state.key == 127 then l else []
            , if state.range == true then r else []
            ]
        )
    , HP.id if state.selected then classes.pianoKeySelected else "not-selected"
    , HE.onClick \_ -> Clicked
    ]
    [ HH.span_ [ HH.text state.text ] ]
    where
    cls = [ HH.ClassName classes.pianoKey ]
    whi = [ HH.ClassName classes.pianoKeyWhite ]
    blk = [ HH.ClassName classes.pianoKeyBlack ]
    l = [ HH.ClassName classes.pianoKeyLast ]
    r = [ HH.ClassName classes.pianoKeyRange ]
    m = [ HH.ClassName classes.pianoKeyMapped ]

  handleAction :: forall m'. KeyAction -> H.HalogenM KeyState KeyAction () KeyMessage m' Unit
  handleAction action = case action of
    ReceiveKey { mapped, selected, range } -> H.modify_ \s -> s { mapped = mapped, selected = selected, range = range }
    Clicked -> do
      state <- H.get
      H.raise $ SendKeynumber state.key

-- the piano is composed of multiple black and white keys
type State =
  { c4 :: Int
  , mapped :: Array Sample
  , selected :: Int
  }

type Slots = (key :: H.Slot (Const Void) KeyMessage Int)
data Action = KeyClicked Int | Receive State
data Message = SendKey Int
_key = Proxy :: Proxy "key"

piano :: forall q m. H.Component q State Message m
piano =
  H.mkComponent
    { initialState: \s -> s
    , render
    , eval: H.mkEval $ H.defaultEval
        { handleAction = handleAction
        , receive = Just <<< Receive
        }
    }
  where
  render :: forall mm. State -> H.ComponentHTML Action Slots mm
  render state = HH.div [ HP.class_ $ HH.ClassName classes.piano ]
    [ HH.ul [ HP.class_ $ HH.ClassName classes.pianoKeys ]
        ( catMaybes $ map
            ( \n ->
                createKey n state.c4 (elem n keys) (n == state.selected) (elem n values) km
            )
            (range 0 127)
        )
    ]
    where
    km = keymap state.c4
    ranges = Map.fromFoldable $ map (\(Sample s) -> Tuple s.keyNumber s.midi) state.mapped
    keys = Set.toUnfoldable $ Map.keys ranges
    values = midiValues $ Map.values ranges

  listen :: KeyMessage -> Action
  listen message = case message of
    SendKeynumber k -> KeyClicked k

  handleAction :: forall m'. Action -> H.HalogenM State Action Slots Message m' Unit
  handleAction action = case action of
    KeyClicked k -> do
      H.modify_ \s -> s { selected = k }
      H.raise $ SendKey k
    Receive { c4, mapped, selected } -> H.modify_ \s -> s { c4 = c4, mapped = mapped, selected = selected }

  createKey :: forall mm. Int -> Int -> Boolean -> Boolean -> Boolean -> KeyMap -> Maybe (HH.HTML (H.ComponentSlot Slots mm Action) Action)
  createKey k c4 mapped selected range km =
    case index km k of
      Just (Note n) -> case n.key of
        Key { name: C, sig: None } -> Just $ HH.slot _key k (pianoKey)
          { key: k
          , color: White
          , mapped: mapped
          , selected: selected
          , range: range
          , text: "C" <> (show $ ((k - c4) / 12) + 4)
          }
          listen
        Key { name: D, sig: None } -> Just $ HH.slot _key k (pianoKey)
          { key: k
          , color: White
          , mapped: mapped
          , selected: selected
          , range: range
          , text: ""
          }
          listen
        Key { name: E, sig: None } -> Just $ HH.slot _key k (pianoKey)
          { key: k
          , color: White
          , mapped: mapped
          , selected: selected
          , range: range
          , text: ""
          }
          listen
        Key { name: F, sig: None } -> Just $ HH.slot _key k (pianoKey)
          { key: k
          , color: White
          , mapped: mapped
          , selected: selected
          , range: range
          , text: ""
          }
          listen
        Key { name: G, sig: None } -> Just $ HH.slot _key k (pianoKey)
          { key: k
          , color: White
          , mapped: mapped
          , selected: selected
          , range: range
          , text: ""
          }
          listen
        Key { name: A, sig: None } -> Just $ HH.slot _key k (pianoKey)
          { key: k
          , color: White
          , mapped: mapped
          , selected: selected
          , range: range
          , text: ""
          }
          listen
        Key { name: B, sig: None } -> Just $ HH.slot _key k (pianoKey)
          { key: k
          , color: White
          , mapped: mapped
          , selected: selected
          , range: range
          , text: ""
          }
          listen
        _ -> Just $ HH.slot _key k (pianoKey)
          { key: k
          , color: Black
          , mapped: mapped
          , selected: selected
          , range: range
          , text: ""
          }
          listen
      Nothing -> Nothing

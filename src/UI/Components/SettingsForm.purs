-- File Name: SettingsForm.purs
-- Description: Global / per sample settings
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 23 Nov 2023 14:07:44
-- Last Modified: 24 Nov 2023 21:25:52

module UI.Components.SettingsForm where

import Prelude

import Data.Map as Map
import Data.Maybe (Maybe(..))
import Data.Tuple (Tuple(..))
import Effect.Class (class MonadEffect)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Soundmap.Writer (SampleSettings)
import UI.Style (classes)
import Web.HTML.Window (prompt)
import Web.UIEvent.MouseEvent (MouseEvent, toUIEvent)
import Web.UIEvent.UIEvent (view)

type State =
  { settings :: SampleSettings
  , target :: Int
  , name :: String
  }

data Action
  = Receive State
  | UpdateSettings String String
  | Remove String
  | Add MouseEvent
  | Finalize

data Message = SendSettings Int SampleSettings

component :: forall q m. MonadEffect m => H.Component q State Message m
component =
  H.mkComponent
    { initialState: \s -> s
    , render
    , eval: H.mkEval $ H.defaultEval
        { handleAction = handleAction
        , receive = Just <<< Receive
        }
    }

field :: forall w. String -> String -> HH.HTML w Action
field key value =
  HH.div [ HP.class_ $ HH.ClassName classes.fieldDiv ]
    [ HH.label [ HP.for key ] [ HH.text key ]
    , HH.input
        [ HP.id key
        , HP.type_ $ HP.InputText
        , HP.value value
        , HE.onValueChange \v -> UpdateSettings key v
        ]
    , HH.button [ HP.classes [ HH.ClassName classes.buttonRemove, HH.ClassName classes.buttonSmall ], HP.title "remove", HE.onClick \_ -> Remove key ] []
    ]

render :: forall m. State -> H.ComponentHTML Action () m
render state = HH.div [ HP.class_ $ HH.ClassName classes.mapBoxInner ]
  [ HH.span_ [ HH.text state.name ]
  , HH.br_
  , HH.div_ $
      map (\(Tuple k v) -> field k v) (Map.toUnfoldable state.settings)
  , HH.button [ HP.classes [ HH.ClassName classes.buttonAdd, HH.ClassName classes.buttonSmall ], HP.title "add opcode", HE.onClick \ev -> Add ev ] []
  ]

handleAction :: forall m. MonadEffect m => Action -> H.HalogenM State Action () Message m Unit
handleAction action = case action of
  Add ev -> case view $ toUIEvent ev of
    Just w -> do
      response <- H.liftEffect $ prompt "Opcode Name:" w
      case response of
        Nothing -> pure unit
        Just "" -> pure unit
        Just res -> do
          state <- H.modify \s -> s { settings = Map.insert res "" s.settings }
          H.raise (SendSettings state.target state.settings)
    Nothing -> pure unit
  Remove key -> do
    state <- H.modify \s -> s { settings = Map.delete key s.settings }
    H.raise (SendSettings state.target state.settings)
  Receive { settings } -> H.modify_ \s -> s { settings = settings }
  UpdateSettings k v -> do
    state <- H.modify \s -> s { settings = Map.update (\_ -> Just v) k s.settings }
    H.raise (SendSettings state.target state.settings)
    H.modify_ \_ -> state
  Finalize -> do
    state <- H.get
    H.raise (SendSettings state.target state.settings)

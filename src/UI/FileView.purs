-- File Name: FileView.purs
-- Description: FileUpload view
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 21 Nov 2023 18:14:01
-- Last Modified: 24 Nov 2023 21:54:00

module UI.FileView where

import Prelude

import DOM.HTML.Indexed.InputAcceptType (InputAcceptTypeAtom(..))
import Data.Maybe (Maybe(..))
import Effect.Aff.Class (class MonadAff)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import UI.Components.HelpButtons (helpButtons)
import UI.Style (classes)
import Web.Event.Event (Event, target)
import Web.File.File (name)
import Web.File.FileList (items)
import Web.HTML.HTMLInputElement (files, fromEventTarget)

data Action = FileChoose Event

data Message
  = Content (Array String)
  | NoContent

component :: forall i m q. MonadAff m => H.Component q i Message m
component =
  H.mkComponent
    { initialState: \_ -> {}
    , render
    , eval: H.mkEval $ H.defaultEval
        { handleAction = handleAction
        }
    }

render :: forall m s. s -> H.ComponentHTML Action () m
render _ =
  HH.div_ [ HH.div [ HP.class_ (HH.ClassName classes.navigation) ] [ helpButtons ], input ]
  where
  input = HH.input
    [ HP.type_ HP.InputFile
    , HP.class_ $ HH.ClassName classes.fileSelector
    , HP.accept $ HP.InputAcceptType
        [ AcceptFileExtension ".wav"
        , AcceptFileExtension ".flac"
        , AcceptFileExtension ".ogg"
        , AcceptFileExtension ".oga"
        , AcceptFileExtension ".aiff"
        , AcceptFileExtension ".aif"
        , AcceptFileExtension ".aifc"
        ]
    , HP.multiple true
    , HE.onInput \ev -> FileChoose ev
    ]

handleAction :: forall m s. MonadAff m => Action -> H.HalogenM s Action () Message m Unit
handleAction action = case action of
  FileChoose ev → case target ev of
    Just t -> case fromEventTarget t of
      Just input -> do
        filelist <- H.liftEffect $ files input
        case filelist of
          Just f -> do
            H.raise $ Content $ map (\e -> name e) (items f)
          Nothing -> H.raise NoContent
      Nothing -> H.raise NoContent
    Nothing -> H.raise NoContent

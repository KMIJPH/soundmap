# soundmap

Soundmap is an automapper for [SFZ](https://sfzformat.com/)
and [DecentSampler](https://www.decentsamples.com/product/decent-sampler-plugin/) instruments
with some extra features.

A running example can be found [here](https://kmijph.codeberg.page/apps/soundmap/).

## How does it work?

The parser will try to obtain information from the file name with `attributes`.
Attributes are values separated by an underscore (`_`).

Examples:

```
some_file_c4_p_1.wav
  │   │   │  │ └ round robin
  │   │   │  └ dynamic
  │   │   └ key number (60)
  │   └ nothing
  └ nothing

flute_60_1.wav
  │   │  └ round robin
  │   └ key number
  └ nothing

asdf_60_gibberish_1_ff.wav
  │   │     │     │  └ dynamic
  │   │     │     └ round robin
  │   │     └ nothing
  │   └ key number
  └ nothing
```

They can be arranged in any order, as long as the keynumber (e.g 60) or key name (e.g C4) comes before round robin,
meaning the first nuber will always be interpreted as the keynumber, the second as the round robin.

If no dynamics are found, the MIDI velocity range defaults to 0-127.  
If no round robins are found, the round robin for a given sample will default to 1.  
The only important information needed is a key number / key name.

# UI

Upon selecting multiple audio files, soundmap will parse information from the file names (audio files will not be read).

## Piano view

- Keys with a sample associated to them are shown in blue
- The currently selected key is shown in light blue
- Keys without sample are grey

Upon clicking on a key, more information on the key will be shown.

## Global Settings

The visibility of global settings can be toggled by clicking on the button on the top.

### Opcodes

Opcodes can be added or removed (globally or per sample) by clicking on the `+` or `x` buttons.
(No input validation is performed!)

### General Settings

`C4 Keynumber` = the MIDI keynumber that corresponds to C4 (middle C) on the keyboard.  
`Stretch Samples` = map/stretch lower and upper MIDI boundaries of samples over the whole keyboard.  
`Output Format` = `.sfz` or `.dspreset`

### Velocity Map

Upper boundaries for MIDI velocities for the 5 supported dynamics can be set independently.  
If the value for a dynamic is deleted it will not be included in the velocity mapping.

If no value for a dynamic is found in the velocity map, the sample will default to using the whole MIDI range (0-127).

# Building

The entire application is written in purescript and can be built with spago:

```bash
spago bundle
```

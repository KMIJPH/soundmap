-- File Name: Writer.purs
-- Description: Writer tests
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 21 Nov 2023 12:30:06
-- Last Modified: 24 Nov 2023 21:49:30

module Test.Writer where

import Data.Maybe (Maybe(..))
import Soundmap.Sample (Dynamic(..), Midi(..), Sample(..))

samples1 :: Array Sample
samples1 =
  [ Sample
      { dynamic: Just P
      , fileName: "sample1.wav"
      , keyNumber: 60
      , seqLength: Just 1
      , roundRobin: Just 1
      , midi: Midi { low: 60, high: 63 }
      , velocity: Midi { low: 0, high: 127 }
      }
  , Sample
      { dynamic: Just P
      , fileName: "sample2.wav"
      , keyNumber: 64
      , seqLength: Just 1
      , roundRobin: Just 1
      , midi: Midi { low: 64, high: 127 }
      , velocity: Midi { low: 0, high: 50 }
      }
  ]

sfz1 :: String
sfz1 =
  """// Automapped using https://codeberg.org/KMIJPH/soundmap
<control>
default_path=./

<global
<group>
ampeg_attack=0.0
ampeg_decay=0.0
ampeg_delay=0.0
ampeg_release=0.001
ampeg_sustain=100.0
loop_mode=no_loop

<region> sample=sample1.wav pitch_keycenter=60 lokey=60 hikey=63 lovel=0 hivel=127 seq_length=1 seq_position=1
<region> sample=sample2.wav pitch_keycenter=64 lokey=64 hikey=127 lovel=0 hivel=50 seq_length=1 seq_position=1"""

ds1 :: String
ds1 =
  """<!-- Automapped using https://codeberg.org/KMIJPH/soundmap -->
<DecentSampler>
  <groups>
    <group ampeg_attack="0.0" ampeg_decay="0.0" ampeg_delay="0.0" ampeg_release="0.001" ampeg_sustain="100.0" loop_mode="no_loop">
      <sample path="./sample1.wav" rootNote="60" loNote="60" hiNote="63" loVel="0" hiVel="127" seqLength="1" seqPosition="1">
      <sample path="./sample2.wav" rootNote="64" loNote="64" hiNote="127" loVel="0" hiVel="50" seqLength="1" seqPosition="1">
    </group>
  </group>
</DecentSampler>"""

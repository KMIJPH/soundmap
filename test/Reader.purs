-- File Name: Reader.purs
-- Description: Reader tests
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 20 Nov 2023 15:52:33
-- Last Modified: 01 Dez 2023 23:07:57

module Test.Reader where

import Prelude

import Data.Either (Either(..))
import Data.Maybe (Maybe(..))
import Parsing (Parser, runParser)
import Soundmap.Sample (Dynamic(..), Midi(..), Sample(..))
import Test.Unit (Test, failure)
import Test.Unit.Assert (equal)

-- | test a single parser
testParser :: forall a. Eq a => Show a => String -> Parser String a -> a -> Test
testParser str p equ =
  case runParser str p of
    Left e -> failure $ "at " <> str <> ": " <> show e
    Right x -> equal x equ

filename1Str :: String
filename1Str = "bongo_c#4_p_3.mp3"

filename1Attrs :: Array String
filename1Attrs = [ "bongo", "c#4", "p", "3", "mp3" ]

filename1Sample :: Maybe Sample
filename1Sample =
  Just $ Sample
    { dynamic: Just P
    , fileName: filename1Str
    , keyNumber: 61
    , roundRobin: Just 3
    , seqLength: Nothing
    , midi: Midi { high: 61, low: 61 }
    , velocity: Midi { low: 0, high: 127 }
    }

filename2Str :: String
filename2Str = "bongo_c4_mp.mp3"

filename2Attrs :: Array String
filename2Attrs = [ "bongo", "c4", "mp", "mp3" ]

filename2Sample :: Maybe Sample
filename2Sample =
  Just $ Sample
    { dynamic: Just MP
    , fileName: filename2Str
    , keyNumber: 60
    , seqLength: Nothing
    , roundRobin: Nothing
    , midi: Midi { high: 60, low: 60 }
    , velocity: Midi { low: 0, high: 127 }
    }

filename3Str :: String
filename3Str = "bongo_1_p.wav"

filename3Sample :: Maybe Sample
filename3Sample =
  Just $ Sample
    { dynamic: Just P
    , fileName: filename3Str
    , keyNumber: 1
    , seqLength: Nothing
    , roundRobin: Nothing
    , midi: Midi { high: 1, low: 1 }
    , velocity: Midi { low: 0, high: 127 }
    }

filename4Str :: String
filename4Str = "bongo_bb3.mp3"

filename4Sample :: Maybe Sample
filename4Sample =
  Just $ Sample
    { dynamic: Nothing
    , fileName: filename4Str
    , keyNumber: 56
    , seqLength: Nothing
    , roundRobin: Nothing
    , midi: Midi { high: 56, low: 56 }
    , velocity: Midi { low: 0, high: 127 }
    }

filename5Str :: String
filename5Str = "A0-f.wav"

filename5Sample :: Maybe Sample
filename5Sample = Nothing

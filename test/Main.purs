-- File Name: Main.purs
-- Description: Tests
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 20 Nov 2023 11:09:56
-- Last Modified: 01 Dez 2023 23:06:35

module Test.Main where

import Prelude

import Data.Map as Map
import Data.Maybe (Maybe(..))
import Effect (Effect)
import Effect.Class (liftEffect)
import Soundmap.Note (Key(..), KeyName(..), Note(..), Signature(..), fromNote, keymap)
import Soundmap.Reader (attrs, keynumber, sample)
import Soundmap.Sample (defaultVelocityMap, fixRobins, mapVelocity, midiMap, midiValues, stretch)
import Soundmap.Sample as S
import Soundmap.Writer (OutputType(..), defaultSettings, write)
import Test.Reader as TR
import Test.Sample as TS
import Test.Unit (test)
import Test.Unit.Assert (equal)
import Test.Unit.Main (runTest)
import Test.Writer as TW

main :: Effect Unit
main = runTest do
  test "eq" do
    equal (Key { name: C, sig: None }) (Key { name: C, sig: None })
    equal (Key { name: C, sig: Flat }) (Key { name: B, sig: None })
    equal (Key { name: C, sig: Sharp }) (Key { name: D, sig: Flat })
    equal (Key { name: G, sig: Sharp }) (Key { name: A, sig: Flat })
    equal (Key { name: F, sig: Flat }) (Key { name: E, sig: None })
    equal (Key { name: E, sig: Sharp }) (Key { name: F, sig: None })
    equal (Key { name: B, sig: Sharp }) (Key { name: C, sig: None })

    equal S.MP S.MF
    equal S.MF S.MF
    equal S.P S.P
    equal (S.P == S.MP) false
    equal (S.F == S.P) false
    equal (S.PP < S.P) true
    equal (S.P < S.MP) true
    equal (S.F > S.MP) true
    equal (S.FF < S.P) false

  test "keymap" do
    equal (fromNote (Note { key: Key { name: B, sig: Flat }, oct: 3, keyNumber: 0 }) (keymap 60)) (Just 58)
    equal (fromNote (Note { key: Key { name: B, sig: None }, oct: 3, keyNumber: 0 }) (keymap 60)) (Just 59)
    equal (fromNote (Note { key: Key { name: C, sig: Flat }, oct: 4, keyNumber: 0 }) (keymap 60)) (Just 59)
    equal (fromNote (Note { key: Key { name: C, sig: None }, oct: 4, keyNumber: 0 }) (keymap 60)) (Just 60)
    equal (fromNote (Note { key: Key { name: B, sig: Sharp }, oct: 3, keyNumber: 0 }) (keymap 60)) (Just 60)
    equal (fromNote (Note { key: Key { name: C, sig: None }, oct: 4, keyNumber: 0 }) (keymap 72)) (Just 72)

  test "velmap" do
    equal (mapVelocity TS.sampleP defaultVelocityMap) TS.sample1
    equal (mapVelocity TS.sampleN defaultVelocityMap) TS.sampleN
    equal (mapVelocity TS.sampleMF defaultVelocityMap) TS.sample2
    TS.fixVelmap1
    TS.fixVelmap2
    TS.fixVelmap3

  test "midimap" do
    equal (midiMap TS.midimap1Arr) TS.midimap1
    equal (midiMap TS.midimap2Arr) TS.midimap2
    equal (midiMap TS.midimap3Arr) TS.midimap3
    equal (stretch TS.sample1 TS.midimap1) TS.midimap1Sample
    equal (midiValues TS.midiValues1) TS.midiValues1Res

  test "roundRobin" do
    equal (fixRobins (Just 5) [ 1, 2, 5, 6 ]) (Just 3)
    equal (fixRobins (Just 2) [ 1, 2, 2, 4 ]) (Just 2)
    equal (fixRobins (Just 6) [ 1, 6, 7, 3 ]) (Just 3)
    equal (fixRobins Nothing [ 1, 6, 7 ]) (Just 4)
    equal (fixRobins Nothing []) (Just 1)

  test "reader" do
    equal (keynumber "60" (keymap 60)) (Just 60)
    equal (keynumber "c4" (keymap 60)) (Just 60)
    TR.testParser TR.filename1Str attrs TR.filename1Attrs
    TR.testParser TR.filename1Str (sample TR.filename1Str (keymap 60)) TR.filename1Sample
    TR.testParser TR.filename2Str attrs TR.filename2Attrs
    TR.testParser TR.filename2Str (sample TR.filename2Str (keymap 60)) TR.filename2Sample
    TR.testParser TR.filename3Str (sample TR.filename3Str (keymap 60)) TR.filename3Sample
    TR.testParser TR.filename4Str (sample TR.filename4Str (keymap 60)) TR.filename4Sample
    TR.testParser TR.filename5Str (sample TR.filename4Str (keymap 60)) TR.filename5Sample

  test "writer" do
    res1 <- liftEffect $ write SFZ defaultSettings TW.samples1 Map.empty
    equal res1 TW.sfz1

    res2 <- liftEffect $ write DecentSampler defaultSettings TW.samples1 Map.empty
    equal res2 TW.ds1

-- File Name: Sample.purs
-- Description: Velocity mapping / midi mapping tests
-- Author: KMIJPH
-- Repository: codeberg.org/KMIJPH
-- License: GPL3
-- Creation Date: 21 Nov 2023 10:25:45
-- Last Modified: 24 Nov 2023 18:30:35

module Test.Sample where

import Prelude

import Data.Array (concat, range, toUnfoldable)
import Data.List (List)
import Data.Map (empty, fromFoldable)
import Data.Maybe (Maybe(..))
import Data.Newtype (un)
import Data.Tuple (Tuple(..))
import Soundmap.Sample (Dynamic(..), Midi(..), MidiMap, Sample(..), fixVelocities)
import Test.Unit (Test)
import Test.Unit.Assert (equal)

sampleP :: Sample
sampleP = Sample
  { dynamic: Just P
  , fileName: ""
  , keyNumber: 0
  , seqLength: Nothing
  , roundRobin: Nothing
  , midi: Midi { low: 0, high: 0 }
  , velocity: Midi { low: 0, high: 0 }
  }

sampleN :: Sample
sampleN = Sample
  { dynamic: Nothing
  , fileName: ""
  , keyNumber: 0
  , seqLength: Nothing
  , roundRobin: Nothing
  , midi: Midi { low: 0, high: 0 }
  , velocity: Midi { low: 0, high: 0 }
  }

sampleMF :: Sample
sampleMF = Sample
  { dynamic: Just MF
  , fileName: ""
  , keyNumber: 0
  , seqLength: Nothing
  , roundRobin: Nothing
  , midi: Midi { low: 0, high: 0 }
  , velocity: Midi { low: 0, high: 0 }
  }

sample1 :: Sample
sample1 = Sample $ s { velocity = Midi { low: 26, high: 51 } }
  where
  s = un Sample sampleP

sample2 :: Sample
sample2 = Sample $ s { velocity = Midi { low: 52, high: 77 } }
  where
  s = un Sample sampleMF

fixVelmap1 :: Test
fixVelmap1 =
  equal
    ( fromFoldable
        [ Tuple P (Midi { low: 0, high: 49 })
        , Tuple MP (Midi { low: 50, high: 99 })
        , Tuple F (Midi { low: 100, high: 127 })
        ]
    )
    $
      fixVelocities
        ( fromFoldable
            [ Tuple P (Midi { low: 0, high: 10 })
            , Tuple MP (Midi { low: 50, high: 80 })
            , Tuple F (Midi { low: 100, high: 127 })
            ]
        )

fixVelmap2 :: Test
fixVelmap2 =
  equal
    ( fromFoldable
        [ Tuple P (Midi { low: 0, high: 127 }) ]
    )
    $
      fixVelocities
        ( fromFoldable
            [ Tuple P (Midi { low: 0, high: 49 }) ]
        )

fixVelmap3 :: Test
fixVelmap3 = equal empty $ fixVelocities empty

midimap1Arr :: Array Int
midimap1Arr = [ 0, 60, 101 ]

midimap1 :: MidiMap
midimap1 =
  fromFoldable kv
  where
  kv =
    [ Tuple 0 (Midi { low: 0, high: 59 })
    , Tuple 60 (Midi { low: 60, high: 100 })
    , Tuple 101 (Midi { low: 101, high: 127 })
    ]

midimap1Sample :: Sample
midimap1Sample = Sample $ s { midi = Midi { low: 0, high: 59 } }
  where
  s = un Sample sample1

midimap2Arr :: Array Int
midimap2Arr = [ 0, 60, 127 ]

midimap2 :: MidiMap
midimap2 =
  fromFoldable kv
  where
  kv =
    [ Tuple 0 (Midi { low: 0, high: 59 })
    , Tuple 60 (Midi { low: 60, high: 126 })
    , Tuple 127 (Midi { low: 127, high: 127 })
    ]

midimap3Arr :: Array Int
midimap3Arr = [ 60 ]

midimap3 :: MidiMap
midimap3 =
  fromFoldable kv
  where
  kv =
    [ Tuple 60 (Midi { low: 0, high: 127 }) ]

midiValues1 :: List Midi
midiValues1 =
  toUnfoldable
    [ Midi { low: 0, high: 12 }
    , Midi { low: 15, high: 17 }
    ]

midiValues1Res :: Array Int
midiValues1Res = concat [ range 0 12, range 15 17 ]
